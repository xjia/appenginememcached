<?php
class Memcached
{
  private $cache = NULL;
  private $Expiration = NULL;
  private $SetPolicy = NULL;

  public function __construct()
  {
    $GAE = 'com.google.appengine.api.memcache.';

    $MemcacheServiceFactory = java_class($GAE . 'MemcacheServiceFactory');
    $ErrorHandlers = java_class($GAE . 'ErrorHandlers');
    $Level = java_class('java.util.logging.Level');

    $this->cache = $MemcacheServiceFactory->getMemcacheService();
    $this->cache->setErrorHandler($ErrorHandlers->getConsistentLogAndContinue($Level->INFO));

    $this->Expiration = java_class($GAE . 'Expiration');
    $this->SetPolicy = java_class($GAE . 'MemcacheService.SetPolicy');
  }

  private function expires(int $ttl)
  {
    if ($ttl) {
      return $this->Expiration->byDeltaSeconds($ttl);
    }
    return NULL;
  }

  /**
   * Tries to set a value to the cache, but stops if a value already exists
   *
   * @param  string  $key    The key to store as, this should not exceed 250 characters
   * @param  mixed   $value  The value to store, this will be serialized
   * @param  integer $ttl    The number of seconds to keep the cache valid for, 0 for no limit
   * @return boolean  If the key/value pair were added successfully
   */
  public function add(string $key, $value, int $ttl)
  {
    return $this->cache->put($key, $value, $this->expires($ttl), $this->SetPolicy->ADD_ONLY_IF_NOT_PRESENT);
  }

  /**
   * Clears the WHOLE cache of every key, use with caution!
   * @return boolean  If the cache was successfully cleared
   */
  public function flush()
  {
    $this->cache->clearAll();
    return TRUE;
  }

  /**
   * Deletes a value from the cache
   *
   * @param  string  $key   The key to be deleted
   * @param  integer $time  The amount of time the server will wait to delete the item
   * @return boolean  If the delete succeeded
   */
  public function delete(string $key, int $time)
  {
    return $this->cache->delete($key, $time);
  }

  /**
   * Returns a value from the cache
   *
   * @param  string $key      The key to return the value for
   * @return mixed  The cached value
   */
  public function get(string $key)
  {
    return $this->cache->get($key);
  }

  /**
   * Sets a value to the cache, overriding any previous value
   *
   * @param  string  $key    The key to store as, this should not exceed 250 characters
   * @param  mixed   $value  The value to store, this will be serialized
   * @param  integer $ttl    The number of seconds to keep the cache valid for, 0 for no limit
   * @return boolean  If the value was successfully saved
   */
  public function set(string $key, $value, int $ttl)
  {
    return $this->cache->put($key, $value, $this->expires($ttl), $this->SetPolicy->SET_ALWAYS);
  }
}
